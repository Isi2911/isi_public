nextflow.enable.dsl = 2

params.kmerlength = 71
params.cov_cutoff = 0
params.min_contig_lgth = 2

params.with_velvet = false
params.with_spades = false


process velvet {
  publishDir "${params.outdir}", mode : "copy", overwrite : true
  container "https://depot.galaxyproject.org/singularity/velvet%3A1.2.10--h5bf99c6_4 "
  input:
    path fastqfiles
  output:
    path "*", emit: velvetfiles
    path "${fastqfiles.getSimpleName()}_velvet.fasta", emit: velvetcontigs
  script:
    """
    velveth out/ ${params.kmerlength} -fastq -short ${fastqfiles} > results
    velvetg out/ -cov_cutoff ${params.cov_cutoff} -min_contig_lgth ${params.min_contig_lgth} 
    cp ./out/*fa ./${fastqfiles.getSimpleName()}_velvet.fasta
    """
}



process spades {
  publishDir "${params.outdir}", mode : "copy", overwrite : true
  container "https://depot.galaxyproject.org/singularity/spades%3A3.15.3--h95f258a_0"
  input:
    path fastqfiles
  output:
    path "*", emit: spadesfiles
    path "${fastqfiles.getSimpleName()}_spades.fasta", emit: spadescontigs
  script:
    """
    spades.py -s ${fastqfiles} -o spades 
    mv ./spades/contigs.fasta ./${fastqfiles.getSimpleName()}_spades.fasta
    """
}

process quast {
  publishDir "${params.outdir}", mode : "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/quast%3A5.0.2--py36pl5262h30a8e3e_4"
  input:
    path contigfiles
  output:
    path  "*"
  script:
    """
    python /usr/local/bin/quast ${contigfiles} 
    """
}


workflow {
  fastqfilechannel = channel.fromPath("${params.indir}/*fastq").collect()
  
  if (params.with_velvet) {
    velvetchannel = velvet(fastqfilechannel)
  }
  
  if (params.with_spades) {
    spadeschannel = spades(fastqfilechannel.flatten())
  }
  v_channel = velvetchannel.velvetcontigs
  s_channel = spadeschannel.spadescontigs
  contigfiles = v_channel.concat(s_channel).collect()
  contigfiles.view()
  quast(contigfiles)
}







