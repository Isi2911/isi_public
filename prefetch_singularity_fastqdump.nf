// Download um fastq-dump erweitern

nextflow.enable.dsl = 2
  
//  container "https://depot.galaxyproject.org/singularity/"

process prefetch {
  storeDir "${params.outdir}"
  container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}/${accession}.sra" // output nun eine datei, statt vorher ein pfad
  script:
    """
    prefetch $accession
    """
}


process fastqDump {
    publishDir "${params.outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
    input:
        path sraresult
    output:
        path "*.fastq"
    script:
    """
    fastq-dump --split-files ${sraresult} > ${sraresult}.splitFilesSRA 
    """
}

process stats {
  publishDir "${params.outdir}/stats", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/ngsutils:0.5.9--py27heb79e2c_4"
  input:
    path stats_channel
  output:
    path "${stats_channel.getSimpleName()}.txt"
  script:
    """
    fastqutils stats ${stats_channel}> ${stats_channel.getSimpleName()}.txt

    """
}

workflow {
  sraresult = prefetch(params.accession)
  //schowFile = showFile(sraresult)
  fastqDump_channel = fastqDump(sraresult)
  stats_channel = stats(fastqDump_channel.flatten())
}

// singularity exec docker://ncbi/sra-tools fastq-dump --split-files SRR1777174





